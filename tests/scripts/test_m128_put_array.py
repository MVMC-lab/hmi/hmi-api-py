import conftest
import serial
import asahmi
import time
import asahmi

# ASA_M128 is connect to COM3

def test_t1():
    hh = asahmi.HmiHandler('COM3', 38400, 1)
    hh.start()

    hh.eventGetArray.wait()
    print("DONE")

if __name__ == '__main__':
    test_t1()

# /**
#  * @file test_array_0.c
#  * @author mickey9910326
#  * @date 2018.05.07
#  * @brief M128 send 5 f32 data.
#  */
#
# #include "../lib/ASA_Lib.h"
# #include "../src/ASA_Lib_HMI.h"
# #include <string.h>
#
# int main() {
#     ASA_M128_set();
#
#     float data[5] = {1.1, -1, 0,1, -2.1};
#     char s[20];
#     char num = 5;
#
#     int bytes = num*sizeof(float); // float is 4 bytes => bytes = 20
#     // NOTE float and double both are 4 bytes (32 bits)
#     // This is the only supported floating point format in AVR-GCC
#
#     M128_HMI_put(bytes, HMI_TYPE_F32, data); // send data
#
#     return 0;
# }
